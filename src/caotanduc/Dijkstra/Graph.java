package caotanduc.Dijkstra;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Graph {

	
	private List<Connection> connections;

	public Graph() {
		super();
	}

	public Graph(List<Connection> connections) {
		super();
		this.connections = connections;
	}

	public List<Connection> getConnections() {
		return connections;
	}

	public void setConnections(List<Connection> connections) {
		this.connections = connections;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((connections == null) ? 0 : connections.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Graph other = (Graph) obj;
		if (connections == null) {
			if (other.connections != null)
				return false;
		} else if (!connections.equals(other.connections))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Graph [connections=" + connections + "]";
	}
	
	public List<Connection> getConnectionsFromNode(int fromNode){
		return connections
				.stream()
				.filter(c -> c.getFromNode()== fromNode)
				.collect(Collectors.toList());		
	}
	
	public void readGraph(String filename){
		try {
			connections = new ArrayList<Connection>();
			Scanner scanner = new Scanner(new File(filename));
			int rowQty = scanner.nextInt();
			for(int i=0; i<rowQty; i++)
			{
				connections.add(new Connection(scanner.nextInt(), scanner.nextInt(), scanner.nextDouble()));
			}
			System.out.println("TD");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
