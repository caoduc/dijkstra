package caotanduc.Dijkstra;

import caotanduc.Dijkstra.Connection;
import caotanduc.Dijkstra.NodeRecord;

public class NodeRecord {

	
	private int node;
	private Connection connection;
	private double costSoFar;
	
	public NodeRecord() {
		super();
	}
	
	

	public NodeRecord(int node, Connection connection, double costSoFar) {
		super();
		this.node = node;
		this.connection = connection;
		this.costSoFar = costSoFar;
	}



	public int getNode() {
		return node;
	}

	public void setNode(int node) {
		this.node = node;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public double getCostSoFar() {
		return costSoFar;
	}

	public void setCostSoFar(double costSoFar) {
		this.costSoFar = costSoFar;
	}

	

	



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((connection == null) ? 0 : connection.hashCode());
		long temp;
		temp = Double.doubleToLongBits(costSoFar);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + node;
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NodeRecord other = (NodeRecord) obj;
		if (connection == null) {
			if (other.connection != null)
				return false;
		} else if (!connection.equals(other.connection))
			return false;
		if (Double.doubleToLongBits(costSoFar) != Double.doubleToLongBits(other.costSoFar))
			return false;
		if (node != other.node)
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "NodeRecord [node=" + node + ", connection=" + connection + ", costSoFar=" + costSoFar + "]";
	}
}
