package caotanduc.Dijkstra;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Dijkstra {

	
	public List<Connection> pathfindDjikstra(Graph graph, int startNode, int endNode){
		
		NodeRecord startRecord = new NodeRecord();
		List<NodeRecord> open = new ArrayList<NodeRecord>();
		List<NodeRecord> closed = new ArrayList<NodeRecord>();
		NodeRecord currentNodeRecord = null;
		NodeRecord endNodeRecord;
		List<Connection> connections;
		double currentEndNodeCost;
		
		
		startRecord.setNode(startNode);
		startRecord.setConnection(null);
		startRecord.setCostSoFar(0);
		
		open.add(startRecord);
		
		while(open.size()>0){
			open.sort(Comparator.comparing(NodeRecord::getCostSoFar));
			
			currentNodeRecord = open.get(0);
			
			if(currentNodeRecord.getNode() == endNode){
				break;
			}
			
			connections = graph.getConnectionsFromNode(currentNodeRecord.getNode());
			
			for(Connection connection : connections){
				int currentEndNode = connection.getToNode();
				currentEndNodeCost = currentNodeRecord.getCostSoFar() + connection.getCost();
				
				if(closed.stream().filter(c -> c.getNode() == currentEndNode).findFirst().isPresent()){
					continue;
				} else if(open.stream().filter(c -> c.getNode() == currentEndNode).findFirst().isPresent()){
					endNodeRecord = open.stream().filter(c -> c.getNode() == currentEndNode).findAny().orElse(null);
					if(endNodeRecord.getCostSoFar() <= currentEndNodeCost){
						continue;
					}
				}else{
					endNodeRecord = new NodeRecord();
					endNodeRecord.setNode(currentEndNode);
				}
				
				endNodeRecord.setCostSoFar(currentEndNodeCost);
				endNodeRecord.setConnection(connection);
				int tempNode = endNodeRecord.getNode();
				if(!open.stream().filter(c -> c.getNode() == tempNode).findFirst().isPresent()){
					open.add(endNodeRecord);
				}
			}
			
			open.remove(currentNodeRecord);
			closed.add(currentNodeRecord);	
		}
		
		if(currentNodeRecord == null || currentNodeRecord.getNode()!=endNode){
			return null;
		}
		
		List<Connection> path = new ArrayList<Connection>();
		
		while(currentNodeRecord.getNode() != startNode){
			path.add(currentNodeRecord.getConnection());
			int tempNode = currentNodeRecord.getConnection().getFromNode(); 
			currentNodeRecord = closed.stream().filter(c -> c.getNode() == tempNode)
					.findAny().orElse(null);
		}
		
		Collections.reverse(path);
		
		return path;
	}
	
	public static void main(String[] args){
		Graph graph = new Graph();
		graph.readGraph("D://aviationfare.txt");
		int startNode = 1;
		int endNode = 7;
		List<Connection> path = new Dijkstra().pathfindDjikstra(graph, startNode, endNode);
		
	}
}
