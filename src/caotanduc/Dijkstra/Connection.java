package caotanduc.Dijkstra;

import caotanduc.Dijkstra.Connection;

public class Connection {

	private int fromNode;
	private int toNode;
	private double cost;
	
	public Connection() {
		super();
	}

	public Connection(int fromNode, int toNode, double cost) {
		super();
		this.fromNode = fromNode;
		this.toNode = toNode;
		this.cost = cost;
	}

	public int getFromNode() {
		return fromNode;
	}

	public void setFromNode(int fromNode) {
		this.fromNode = fromNode;
	}

	public int getToNode() {
		return toNode;
	}

	public void setToNode(int toNode) {
		this.toNode = toNode;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(cost);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + fromNode;
		result = prime * result + toNode;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Connection other = (Connection) obj;
		if (Double.doubleToLongBits(cost) != Double.doubleToLongBits(other.cost))
			return false;
		if (fromNode != other.fromNode)
			return false;
		if (toNode != other.toNode)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Connection [fromNode=" + fromNode + ", toNode=" + toNode + ", cost=" + cost + "]";
	}
}
